function [y, inds] = vl_nnmaxout(x,groupSize,inds,dzdy)
% VL_NNMAXOUT CNN maxout layer
%   Y = VL_NNMAXOUT(X) applies maxout to the data X, taking the maximum 
%   over all channels.
%
%   [Y, inds] = VL_NNMAXOUT(X, groupSize) splits X channels into groups of
%   groupSize and returns the maximum response in each group. inds should be
%   a vector of indices to be passed as an argument in vl_nnmaxout during the
%   backpropagation step and its purpose is to determine which elements of
%   the derivative dz/dy will be used to compute dz/dx.
%
%   Y = VL_NNMAXOUT(X, groupSize, inds, dzdy) computes the
%   derivatives DZDX of the network relative to the input X given the 
%   the derivative DZDY relative to the outut Y and the indices
%   corresponding to maximum elements from the forward step.

sz = size(x);
if nargin < 2, groupSize = sz(3); end
forward = (nargin < 3);

if mod(sz(3),groupSize) ~= 0
    error('groupSize does not divide the number of channels')
end

nGroups = sz(3)/groupSize;
if forward
  y = zeros(sz(1), sz(2),nGroups, sz(4),'single');
  for i = 1:nGroups
    [y(:,:,i,:), indsInt] = max(x(:,:,(i-1)*groupSize+1:i*groupSize,:),[],3);
    if i == 1
      inds = indsInt;
    else
      inds = cat(3,inds,indsInt);
    end
  end
else
  y = zeros(size(x),'single');
  for i = 1:nGroups
    for k = 1:sz(1)
      for l = 1:sz(2)
        for j = 1:sz(4)
          y(k,l,(i-1)*groupSize+inds(k,l,i,j),j) = dzdy(k,l,i,j);
        end
      end
    end
  end
end