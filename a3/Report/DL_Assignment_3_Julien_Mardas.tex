\documentclass[paper=a4, fontsize=11pt]{scrartcl}
\usepackage[T1]{fontenc} 
\usepackage[english]{babel} 
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amsthm}
\usepackage{fancyhdr} 
\usepackage{sectsty}
\usepackage{float}
\usepackage{graphicx}
\usepackage{booktabs}
\usepackage{caption}
\usepackage{subcaption}
\usepackage[]{hyperref}
\usepackage{listings}
\usepackage[framed]{matlab-prettifier}

\allsectionsfont{\normalfont \bfseries}
\pagestyle{fancyplain}
\fancyhead{}
\fancyfoot[L]{}
\fancyfoot[C]{\thepage}
\fancyfoot[R]{}
\renewcommand{\headrulewidth}{0pt}
\renewcommand{\footrulewidth}{0pt}
\setlength{\headheight}{14pt}
\setlength\parindent{0pt} 

\newcommand{\horrule}[1]{\rule{\linewidth}{#1}} % Create horizontal rule command with 1 argument of height

\title{ 
    \normalfont 
    \text{Ecole Centrale Paris - MSc Data Science } \\ [20pt]
    \horrule{2pt} \\[0.5cm] 
    \huge \bfseries{Deep Learning Assignment 3} \\ 
    \horrule{2pt} \\[0.5cm]
}
\author{Julien Mardas}
\date{September 14, 2016}

\DeclareMathOperator*{\minimize}{minimize}


\begin{document}
\maketitle

%----------------------------------------------------------------------------------------
%   Analytical exercise
%----------------------------------------------------------------------------------------

\section{Dropout for linear regression}

Given a Neural Network where $X \in \mathbb{R}^d$ and $y$ are respectively the input and the output of a Dropout layer and, $w$ the associated weighted matrix, we want to minimize the objective function:
\begin{align*}
    \left \| y - Xw \right \|^2    
\end{align*}
\par\medskip

We assume in the Dropout layer that each dimension of is said to be dropped out if it is retained with a probability $p$. 
\par\medskip 

Hence, the input can be expressed as $R * X$ where $R\in\{0,1\}^{N\times D}$ is a random matrix of which each elements $R_{ij}$ are following a Bernoulli distribution of probability $p$.
\par\medskip

The operator $*$ is the Hadamard element-wise product $(R*X)_{ij}=(r_{ij})(x_{ij})$. Under those assumptions, the objective function we want to minimize becomes:
\begin{align}
    \minimize_{w} \mathbb{E}_{R \sim B(p)}\left[ 
        \left \| y - (R * X)w \right \|^2
        \right]
\end{align}
\par\medskip

Let's find an expression of the objective function. Assuming $R \sim B(P)$, We have:
\par\medskip

\begin{align} 
\mathbb{E}_{R \sim B(p)}\left[\left \| y - (R * X)w \right \|^2\right] 
    &=  \mathbb{E} \left [ y^Ty - 2y^T(R * X)w +  w^T (R * X)^T (R * X)w \right ]\\
    &= y^Ty - 2y^T \mathbb{E} \left[ (R * X)w \right] + \mathbb{E} \left[ w^T (R * X)^T (R * X)w \right]  \\
    &= y^Ty - 2py^TXw + \mathbb{E} \left[ w^T (R * X)^T (R * X)w \right]
\end{align}

$\mathbb{E}_{R \sim B(p)} \left[ (R * X)w \right] = pXw $ because the $r_{ij}$ are independent and follow a Bernoulli distribution of probability $p$.
Now let's compute the expectation of the third term in equation~(4).
\par\medskip

Let 
\begin{align}
\mathbb{E} \left[ A \right] 
    &= \mathbb{E}_{R \sim B(p)} \left[ (R * X)^T (R * X)\right] \\
    &= \mathbb{E} \left[ \sum_{k=1}^{N} r_{ik}x_{ik}r_{jk}x_{jk}\right] \\
    &= \sum_{k=1, i \neq j}^{N} \mathbb{E} \left[r_{ik}\right]\mathbb{E} \left[r_{jk}\right]x_{ik}x_{jk} + \sum_{k=1, i=j}^{N} \mathbb{E} \left[r^{2}_{ik}\right]x^{2}_{ik} \\
    &= \sum_{k=1, i \neq j}^{N} p^2 x_{ik} x_{jk} + \sum_{k=1, i=j}^{N}  p^2 x^{2}_{ii} \\
    &= \sum_{k=1, i \neq j}^{N} p^2 x_{ik} x_{jk} + \sum_{k=1}^{N} p^2 x^{2}_{ik} + p(1-p)x_{ik} \\
    &= p^2 X^T X + p(1-p)\Gamma^2, \text{With, }\Gamma = diag(X^TX)^{1/2}
\end{align}
\par\medskip

As a consequence, using equation (9), we have
\begin{align}
 \mathbb{E} \left[ w^T (R * X)^T (R * X)w \right] 
    &= w^T \mathbb{E} \left[ A \right] w \\
    &= w^T(p^2 X^T X + p(1-p)\Gamma^2)w \\
    &= w^T (p^2 X^T X) w + p(1-p) w^T \Gamma^2 w
\end{align}
\par\medskip

Using (13) in equation (4) we have, 
\begin{align}
\mathbb{E}_{R \sim B(p)}\left[\left \| y - (R * X)w \right \|^2\right] 
    &= y^Ty - 2py^TXw + w^T (p^2 X^T X) w + p(1-p) w^T \Gamma^2 w \\
    &=  \left \| y - pXw \right \|^2 + p(1-p) \left \| \Gamma w \right \|^2 
\end{align}
\par\medskip

Finally, to minimize the expected squared loss of a Dropout layer  marginalizing the noise and under a Bernoulli distribution , we have need to solve equation (16) as follows:

\begin{align}
    \minimize_w \mathbb{E}_{R \sim B(p)}\left[\left \| y - (R * X)w \right \|^2\right]     
        &= \minimize_w \left[ \left \| y - pXw \right \|^2 + p(1-p) \left \| \Gamma w \right \|^2 \right] \qed
\end{align}
\par\medskip
having, $\Gamma = diag(X^TX)^{1/2} $


%----------------------------------------------------------------------------------------
%   Matlab / MatConvNet setup
%----------------------------------------------------------------------------------------
\newpage

\section{Matlab / MatConvNet setup}
In this section we will implement different CNN Layers one of which the dropout layer. The code will be based on the latest MatConvNet library from Mathworks which is at the time of the present writing version 1.0-beta23 \cite{vedaldi15matconvnet}. We adapted. The following files will be used to conduct our experimentaitons:

\begin{itemize}
    
    \item cnn\_mnist\_DLclass.m is a modified version of \$MATHLAB\_HOME/matconvnet-1.0-beta23/examples/mnist/cnn\_mnist.m used to setup and train a CNN on the MNIST database. it defines the layers of
    a typical Yann Lecun CNN (LeNet) and downloads its dataset. This modified version of cnn\_mnist.m uses a subset of the MNIST database and enables GPU parallel CUDA computing (GeForce GTX770) to speed up experiments.
    
    \item cnn\_train.m extracts features and feeds training data to the optimization algorithm. The optimization scheme used is a mini-batch version of Stochastic Gradient Descent (SGD), combined with weight decay and momentum. This function also stores intermediate results, keeps record of the train and validation errors and plots their values for a hundred of epoch. 
    
    \item vl\_simplenn.m: Is the implementation of our output functions and derivatives for every type of layer used in the CNN.
\end{itemize}
\par\medskip

%----------------------------------------------------------------------------------------
%   CNN layers and dropout
%----------------------------------------------------------------------------------------
\newpage

\section{CNN layers and dropout}
\bigskip

\subsection{LeNet on MNIST dataset}
\medskip
The original test is a classic CNN MNIST (LeNet). The figure \ref{XP0} shows the the objective and errors for a 100 epoch.
\par\medskip

\begin{figure}[H]
    \centering
    \includegraphics[scale=0.29]{Results/Orig_objective_vs_error_per_epoch.png}
    \caption{Original LeNet on MNIST dataset objective/errors vs epoch}
    \label{XP0}
\end{figure}\textbf{}
\par\medskip

\subsection{Increasing the depth of the network}
\medskip
Now we add a pair of convolutional and pooling layers after the first pooling layer. with a 3x3 filter and 50 channels. We change the filter size of the two following convolutional layers giving them a 2x2 size. The figure \ref{XP1} shows the the objective and errors for a 100 epoch. 
\par\medskip

\begin{figure}[H]
    \centering
    \includegraphics[scale=0.29]{Results/XP1_objective_vs_error_per_epoch.png}
    \caption{Adding a convolutional layer}
    \label{XP1}
\end{figure}
\par\medskip

\subsection{Reducing the depth of the network}
\medskip
Now we remove the second pair of convolutional and pooling layers and set the last convolutional layer's filter size to 15x15 and channels' to 20. The figure \ref{XP2} shows the the objective and errors for a 100 epoch. 
\par\medskip

\begin{figure}[H]
    \centering
    \includegraphics[scale=0.29]{Results/XP2_objective_vs_error_per_epoch.png}
    \caption{Removing a convolutional layer}
    \label{XP2}
\end{figure}
\par\medskip

We changed the last convolutional layer's filter size to 10x10 with the same setup The figure \ref{XP3} shows the sames objective and validation errors for a 100 epoch.

\begin{figure}[H]
    \centering
    \includegraphics[scale=0.29]{Results/XP3_objective_vs_error_per_epoch.png}
    \caption{removing a convolutional layer}
    \label{XP3}
\end{figure}
\par\medskip

\bigskip
\subsection{Adding a dropout layer}
\bigskip

Now we remove a dropout layer after the ReLu layer with a default droping probability of 0.5. The goal is to avoid having complex fully connected networks and thus overfitting. The figure \ref{XP4} shows the the objective and errors for a 100 epoch. 
\par\medskip

\begin{figure}[H]
    \centering
    \includegraphics[scale=0.29]{Results/XP4_objective_vs_error_per_epoch.png}
    \caption{Objective/erros with a dropout layer to LeNet on MNIST dataset}
    \label{XP4}
\end{figure}
\par\medskip

\subsection{Comparison of the validation errors}
\medskip

As we can see in the table \ref{XP012_val_error}, we had worst results in both of our experiments than in with the original CNN MNIST. 
\par\medskip

In the first experience adding a convolutional layer to our neural network proved to be inefficient. As our dataset is reduced and simple and, our neural network too complex, we don't see the aggravation.
With the complete dataset, we could clearly see that we are overfitting. When a large feedforward neural network is trained on a small training set, it typically performs poorly on held-out test data \cite{hinton13}
\par\medskip

In the second experience, the error rate went even worst. As a matter of fact, brutally increasing the filter size from 5x5  in the previous layer to a 12x12 had the effect of decreasing the depth of the analysis and simplifying to much the model making it consider the hole image to early. Table \ref{XP012_val_error} shows a 2,47\% validation error as opposed to 1,87\% with the original CNN.
\par\medskip

As a matter of fact, changing the filter size in the range $ 10x10 \rightarrow 15x15$ confirming our assumption. Values close to 10x10 took even more time probably due to the fact that the GPU VRAM was hitting its limit but the validation error was lower although the objective is unstable at the beginning.
\par\medskip

For our last experience, we add a dropout layer after the ReLu. The dropout model is a regularization technique in which a random subnetwork is trained at every iteration. The weights of the different networks are then averaged \cite{srivastava14}. 
\par\medskip

This model clearly proved to be effective. Although the learning rate is slower the validation errors significantly dropped. As we are exploring more parts of the network we are avoiding fully connected networks making our learning more efficient over time.
\par\medskip


\begin{table}[H]
\centering
\begin{tabular}{@{}cccc@{}}
\toprule
Original CNN & LeNet+conv Layer & LeNet-conv Layer & LeNet+Dropout \\ \midrule \midrule
1,87 \%      & 2,33 \%          & 2,47 \%          & 1,49 \%       \\ \bottomrule
\end{tabular}
\caption{Validation errors of the three experiments after 100 epoch}
\label{XP012_val_error}
\end{table}


%----------------------------------------------------------------------------------------
%   Maxout implementation
%----------------------------------------------------------------------------------------
\newpage

\section{Maxout implementation}
\bigskip

A Maxout layer is a layer where the activation function is the max of the inputs. The model is simply a feed-forward architecture that uses the following activation function so called a maxout unit. Given $x\in \mathbb{R}^d$ the output of a so called maxout hidden layer will be\cite{bengio13goodfellow}:

\begin{align*}
    h_i(x) = max_{j \in \left[ 1,k \right] } z_{ij}
\end{align*}
\par\medskip

\subsection{Maxout layer implementation}
\bigskip

The code of the Maxout layer can be found in listing \ref{vl_nnmaxout}. The steps for the implementation are:

\begin{enumerate}
    \item divide the feature channels into groups of a given size g
    \item take the maximum feature x in each of the groups separately
    \item compute the derivative of the maxout layer for the back-propagation step.
\end{enumerate}


\lstset{
  style              = Matlab-editor,
  basicstyle         = \mlttfamily,
  escapechar         = ",
  mlshowsectionrules = true,
}
\begin{lstlisting}[basicstyle=\tiny, captionpos=b, caption = {Maxout hidden layer implementation} \label{vl_nnmaxout}]{vl_nnmaxout.m}
function [y, inds] = vl_nnmaxout(x,groupSize,inds,dzdy)
sz = size(x);
if nargin < 2, groupSize = sz(3); end
forward = (nargin < 3);

if mod(sz(3),groupSize) ~= 0
    error('groupSize does not divide the number of channels')
end

nGroups = sz(3)/groupSize;
if forward
  y = zeros(sz(1), sz(2),nGroups, sz(4),'single');
  for i = 1:nGroups
    [y(:,:,i,:), inds_init] = max(x(:,:,(i-1)*groupSize+1:i*groupSize,:),[],3);
    if i == 1
      inds = inds_init;
    else
      inds = cat(3,inds,inds_init);
    end
  end
else
  y = zeros(size(x),'single');
  for i = 1:nGroups
    for k = 1:sz(1)
      for l = 1:sz(2)
        for j = 1:sz(4)
          y(k,l,(i-1)*groupSize+inds(k,l,i,j),j) = dzdy(k,l,i,j);
        end
      end
    end
  end
end
\end{lstlisting}


\subsection{Maxout layer optimal placement}
\bigskip

We will try to place our maxout layer in different place on our neural network:
\begin{itemize}
    \item after the second pooling layer 
    \item after the first pooling layer
\end{itemize}
\par\medskip
For this test, we will be using 2 groups ($g=2$). Results shown in table \ref{maxout_pos} are displaying significant improvement. While dropout proved to be a good approximation to model averaging for deep models, the maxout layer learns relation between layers \cite{bengio13goodfellow}. Learning seems more efficient in the first layer rather than in deepest layers. It seems logic at first glance since small elements like lines, arches, circles (...) may be used to compose more complex objects. Maxout acts in favor of such correlations. 
\par\medskip


\begin{table}[H]
\centering
\begin{tabular}{@{}ccc@{}}
\toprule
Original CNN & Maxout after 1st pooling & Maxout after 2nd pooling \\ \midrule \midrule
1,87 \%      & 1,13 \%                  & 1,74 \%                  \\ \bottomrule
\end{tabular}
\caption{Validation error with variable maxout layer positioning in a CNN on MNIST}
\label{maxout_pos}
\end{table}
\par\medskip


Table \ref{maxout_groups} shows that using four groups gave good results compared to the original net but worst than with a maxout layer of two groups. Dividing the first layer's output into 4 group made it inefficient with two many output channels as relevant results of one group might have been discriminated by slightly better values in the same group.
\begin{table}[H]
\centering
\begin{tabular}{@{}ccc@{}}
\toprule
Original CNN & Maxout $g=2$ & Maxout $g=2$  \\ \midrule \midrule
1,87 \%      & 1,13 \%      & 1,48  \%      \\ \bottomrule
\end{tabular}
\caption{Validation error for a maxout layer with two and for groups}
\label{maxout_groups}
\end{table}




\bigskip
\subsection{Combining dropout and maxout layers}
\bigskip

Now we combine our best results so far with the dropout and maxout layers, which is a maxout unit with two groups after the first convolutional/max pooling layer and a dropout layer (again with $p = 0.5$).
\par\medskip
The result in table \ref{maxout_dropout} proved our assumption to be true as we significantly decreased the validation error.

\begin{table}[H]
\centering
\begin{tabular}{@{}cccc@{}}
\toprule
Original CNN & Dropout & Maxout $g=2$ & Maxout $g=2$ with dropout  \\ \midrule \midrule
1,87 \%      & 1,49 \% & 1,48 \%      & 1,15 \%                     \\ \bottomrule
\end{tabular}
\caption{Combined performance of maxout and dropout layers}
\label{maxout_dropout}
\end{table}

\par\medskip
Maxout is proven to be suited for training with dropout \cite{bengio13goodfellow} \cite{srivastava14}.
Maxout exploits dropout's model averaging behavior. The maxout layer propagates variations in the gradient due dropout masks to the lowest layers of a network, ensuring that every parameter in the model can enjoy
the full benefit of dropout.
\newpage

\bibliography{bibliography.bib}{}
\bibliographystyle{plain}



\end{document}