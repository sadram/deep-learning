[Dtrain,Dtest]  = load_digit7;
whos
[nsamples,ndimensions] = size(Dtrain);

meanDigit =0;
for k=1:nsamples
    meanDigit = meanDigit + Dtrain(k,:)/nsamples;
end

%% simpler & faster
meanDigit = mean(Dtrain,1)';
meanImage = reshape(meanDigit,[28,28]);
figure,imshow(meanImage);

%% Compute the covariance
covDigits = 0;
for k=1:nsamples
    covDigits = covDigits + (Dtrain(k,:)'-meanDigit)*(Dtrain(k,:)'-meanDigit)'/(nsamples-1);
end
covDigitsMatlab = cov(Dtrain);

%% make sure covDigitsMatlab = your covDigits
figure,imagesc(covDigits)
figure,imagesc(covDigitsMatlab)
%We can assume that those two matrix are equal with respect to this error. 
error = sqrt(sum(sum((covDigits -covDigitsMatlab).^2)))/sqrt((sum(sum(covDigits.^2))))

%% get top-5 eigenvectors
[eigvec,eigvals] = eigs(covDigits,5);
figure,
subplot(1,3,1); imshow(reshape(eigvec(:,1),[28,28]),[])
subplot(1,3,2); imshow(reshape(eigvec(:,2),[28,28]),[])
subplot(1,3,3); imshow(reshape(eigvec(:,3),[28,28]),[])
for basis_idx = 1:3
    factors =[-2,0,2];
    figure,
    for k=1:3
        imshow(reshape(meanDigit + 2*factors(k)*eigvec(:,basis_idx),[28,28]))
        % pause for debugging
    end
end

%% Computation of the ACP approximation 
% in function of the number of eigenvectors
D=10;
numtest = size(Dtest,1);
covDigits = cov(Dtrain);
[eigvec,eigvals] = eigs(covDigits,D);
factors = (Dtest-repmat(meanDigit',numtest,1))*eigvec;
E = zeros(D,1);

for j=1:D
    ACP = factors(:,1:j)*(eigvec(:,1:j)');
    tmpResulst = Dtest - repmat(meanDigit',numtest,1);
    tmpResulst = tmpResulst - ACP;
    E(j) = mean(sqrt(sum(tmpResulst.^2,2))); %Here we use the mean instead of the sum
end
figure, plot(E)
title('ACP approximation')
ylabel('error')
xlabel('number of eigen vector')

%% K-means computation with variations of k {2:5, 10, 50, 100}
% let's iterate for the asked value to see if the distortion function
% lowers until a its lower bound. The more we iterate, the more we should
% be precise
for k=[2:5 10 50 100]
    n =15;
    numtest = size(Dtest,1);
    dist = ones(1,10);
    vectKmeans = zeros(k,size(Dtest,2),10);
    dist = zeros(numtest,k,10);
    distortion = zeros(n,10);

    % computed in 5 minuites on my machine (octo-core 32GB of ram, sorry:)
    for i=1:10
        vectKmeans(:,:,i) = Dtest(randi(size(Dtest,1),1,k),:);
        for j=1:n
            for l =1:k
            dist(:,l,i) = sqrt(sum((Dtest-repmat(vectKmeans(l,:,i),size(Dtest,1),1)).^2,2));
            end
            [val ind] = min(dist(:,:,i)');
            for l=1:k
                bool = (ind==l);
                vectKmeans(l,:,i) = mean(Dtest(bool,:));
                distortion(j,i) = distortion(j,i) + sum(dist(bool,l,i));
            end
        end
    end
    %% figure artecfacts
    hold on
    figure(100) 
    plot(distortion(:,in))
    legend('k=2','k=3','k=4','k=5','k=10','k=50','k=100')
    title('K-means iterations')
    ylabel('Distortion')
    xlabel('iteration')
end

hold off
[v, in] = min(distortion(n,:));
