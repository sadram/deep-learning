function[A,dist]=K_mean(Set,K,epsilon)
id=1+floor(rand(size(Set,1),1)*K);
Diff=zeros(size(Set,1),1);
m=zeros(K,size(Set,2));
Cold=0;Cnew=0;
Cnew=0;
for i=1:K
  m(i,:)=mean(Set(id==i,:));
  B=Set(id==i,:)-repmat(m(i,:),size(Set(id==i,:),1),1);
  B=B.*B;
  Cnew=sum(sum(B))+Cnew;
end  
Hist=[Cnew];
while(abs(Cnew-Cold)>epsilon)
    Cold=Cnew;
    for j=1:size(Set,1)
      for i=1:K  
          Z=repmat(Set(j,:),K,1)-m;
          Z=Z.*Z;
          [Diff(j,1),id(j,1)]=min(sum(Z,2));
      end    
    end
    Cnew=0;
    for i=1:K
      m(i,:)=mean(Set(id==i,:));
      B=Set(id==i,:)-repmat(m(i,:),size(Set(id==i,:),1),1);
      B=B.*B;
      Cnew=sum(sum(B))+Cnew;
    end
    Hist=cat(2,Hist,[Cnew]);
end
dist=Hist;
A=id;
end



