figure, ha = axes;
legend_strings = {'ising','bm','rbm'};
plot(info_1.P_ok(:,1:200),'k','linewidth',2);
hold on
plot(info_2.P_ok(:,1:200),'r','linewidth',2);
hold on,
plot(info_3.P_ok(:,1:200),'g','linewidth',2);
legend(legend_strings,'location','southeast','fontsize',20);
xlabel('Gradient ascent iteration','fontsize',20);
ylabel('Network Precision','fontsize',20)
set(ha,'fontsize',20);

figure, ha = axes;
legend_strings = {'ising','bm','rbm'};
plot(info_1.P_ok,'k','linewidth',2);
hold on
plot(info_2.P_ok,'r','linewidth',2);
hold on,
plot(info_3.P_ok,'g','linewidth',2);
legend(legend_strings,'location','southeast','fontsize',20);
xlabel('Gradient ascent iteration','fontsize',20);
ylabel('Network Precision','fontsize',20)
set(ha,'fontsize',20);