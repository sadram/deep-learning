function [x_out, P_h]  = block_gibbs(x_in, W, L)
%
% block_gibbs(obs_K,W_inter,K)
%
%   Performs Block-Gibbs sampling 
%   using x^0, W, L as input 
%   outputing x^L, P(h|x^L;W) with 8 hidden variables (M=8) 
%
%  Arguments: (obs_K, W_inter, K)
%   x_in:       observed input
%   W:          interaction matrix weights (parameter of the model
%   L:          Contrative Divergence
%
%  Returns: (obs_K, P_hidden_K)
%   x_out:      hidden variable
%   P_h:        probability of hidden to be equal to 1

    x_out = x_in;
    h = zeros(size(W,2),1);

    for i = 1:L
        proba   =   1./(1+exp(-2*x_out*W));
        h       =   2*(proba'>rand(size(h,1),1))-1 ;
        proba   =   1./(1+exp(-2*W*h));
        x_out   =   2*(proba'>rand(1,size(x_in,2)))-1 ;
    end
    
    P_h = 1./(1+exp(-2*x_out*W));
end

